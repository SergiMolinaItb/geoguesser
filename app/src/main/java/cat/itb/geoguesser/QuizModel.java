package cat.itb.geoguesser;

public class QuizModel {

    private String qQuestion;
    private String qAnswer;
    private int qChoice;

    public QuizModel(String qQuestion, String qAnswer, int qChoice) {
        this.qQuestion = qQuestion;
        this.qAnswer = qAnswer;
        this.qChoice = qChoice;
    }

    private String[][] qChoices = {
            {"Lisboa", "Copenhaguen", "Madrid", "Brusel·les"},
            {"Sarajevo", "Berlín", "Lisboa", "Reykjavík"},
            {"Paris", "Copenhaguen", "Madrid", "Berlín"},
            {"Madrid", "Paris", "Reykjavík", "Roma"},
            {"Roma", "Sarajevo", "Berlín", "Roma"},
            {"Berlín", "Reykjavík", "Atenes", "Sarajevo"},
            {"Atenes", "Reykjavíc", "Roma", "Lisboa"},
            {"Reykjavík", "Brusel·les", "Roma", "Paris"},
            {"Brusel·les", "Sarajevo", "Madrid", "Lisboa"},
            {"Copenhaguen", "Lisboa", "Roma", "Madrid"},
    };


    public String getQuestion(){
        return qQuestion;
    }

    public String getAnswer(){
        return qAnswer;
    }

    public int getqChoice(){
        return qChoice;
    }

    public String getChoice1 (int i) {
        return qChoices[i-1][0];
    }

    public String getChoice2 (int i) {
        return qChoices[i-1][1];
}

    public String getChoice3 (int i) {
        return qChoices[i-1][2];
    }

    public String getChoice4 (int i) {
        return qChoices[i-1][3];
    }
}
