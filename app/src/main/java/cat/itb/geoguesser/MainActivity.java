package cat.itb.geoguesser;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView titleCountry;
    ImageView imageCountry;
    Button answer1;
    Button answer2;
    Button answer3;
    Button answer4;
    ProgressBar progressBar;
    int i = 0;
    int posicio = 0;

    private QuizModel[] questions = new QuizModel[]{
            new QuizModel("Bèlgica", "Brusel·les", 1),
            new QuizModel("Bòsnia i Herzegovina", "Sarajevo", 2),
            new QuizModel("Dinamarca", "Copenhaguen", 3),
            new QuizModel("França", "París", 4),
            new QuizModel("Alemanya", "Berlín", 5),
            new QuizModel("Grècia", "Atenes", 6),
            new QuizModel("Islàndia", "Reykjavíc", 7),
            new QuizModel("Itàlia", "Roma", 8),
            new QuizModel("Portugal", "Lisboa", 9),
            new QuizModel("Espanya", "Madrid", 10),
    };

    @Override
    protected void onStart() {
        Collections.shuffle(Arrays.asList(questions));
        moveToNextQuestion();
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleCountry = findViewById(R.id.titleCountry);
        imageCountry = findViewById(R.id.fotoQuiz);
        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        answer4 = findViewById(R.id.answer4);
        progressBar = findViewById(R.id.progressBar);
        moveToNextQuestion();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(v);
                i++;
                refreshScreen();
            }
        };

        answer1.setOnClickListener(listener);
        answer2.setOnClickListener(listener);
        answer3.setOnClickListener(listener);
        answer4.setOnClickListener(listener);

        refreshScreen();
    }

    @SuppressLint("SetTextI18n")
    public void refreshScreen(){
        if (posicio == 10) {
            onStart();
        }
        moveToNextQuestion();
        progressBar.incrementProgressBy(10);
    }

    @SuppressLint("SetTextI18n")
    public void moveToNextQuestion(){
        posicio++;
        switch (questions[i].getQuestion()) {
            case "Bèlgica":
                titleCountry.setText("Bèlgica");
                imageCountry.setImageResource(R.drawable.bel);
                setButtons(i);
                break;
            case "Bòsnia i Herzegovina":
                titleCountry.setText("Bòsnia i Herzegovina");
                imageCountry.setImageResource(R.drawable.bos);
                setButtons(i);
                break;
            case "Dinamarca":
                titleCountry.setText("Dinamarca");
                imageCountry.setImageResource(R.drawable.din);
                setButtons(i);
                break;
            case "França":
                titleCountry.setText("França");
                imageCountry.setImageResource(R.drawable.fra);
                setButtons(i);
                break;
            case "Alemanya":
                titleCountry.setText("Alemanya");
                imageCountry.setImageResource(R.drawable.ale);
                setButtons(i);
                break;
            case "Grècia":
                titleCountry.setText("Grècia");
                imageCountry.setImageResource(R.drawable.gre);
                setButtons(i);
                break;
            case "Islàndia":
                titleCountry.setText("Islàndia");
                imageCountry.setImageResource(R.drawable.isl);
                setButtons(i);
                break;
            case "Itàlia":
                titleCountry.setText("Itàlia");
                imageCountry.setImageResource(R.drawable.ita);
                setButtons(i);
                break;
            case "Portugal":
                titleCountry.setText("Portugal");
                imageCountry.setImageResource(R.drawable.por);
                setButtons(i);
                break;
            case "Espanya":
                titleCountry.setText("Espanya");
                imageCountry.setImageResource(R.drawable.esp);
                setButtons(i);
                break;
        }
    }

    public void setButtons(int i){
        answer1.setText(questions[i].getChoice1(questions[i].getqChoice()));
        answer2.setText(questions[i].getChoice2(questions[i].getqChoice()));
        answer3.setText(questions[i].getChoice3(questions[i].getqChoice()));
        answer4.setText(questions[i].getChoice4(questions[i].getqChoice()));
    }

    public void checkAnswer(View v){
        switch (v.getId()){
            case R.id.answer1:
                if (answer1.getText().equals(questions[i].getAnswer())) {
                    Toast.makeText(this, "Resposta Correcta!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Resposta Incorrecta.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.answer2:
                if (answer2.getText().equals(questions[i].getAnswer())) {
                    Toast.makeText(this, "Resposta Correcta!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Resposta Incorrecta.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.answer3:
                if (answer3.getText().equals(questions[i].getAnswer())) {
                    Toast.makeText(this, "Resposta Correcta!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Resposta Incorrecta.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.answer4:
                if (answer4.getText().equals(questions[i].getAnswer())) {
                    Toast.makeText(this, "Resposta Correcta!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Resposta Incorrecta.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}